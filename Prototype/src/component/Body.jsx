// @flow
import * as React from 'react';

type Props = {
  children?: React.Node,
};

function Body(props: Props): React.Node {
  const { children } = props;
  return <div className="main">{ children }</div>;
}

Body.defaultProps = {
  children: null,
};

export default Body;
