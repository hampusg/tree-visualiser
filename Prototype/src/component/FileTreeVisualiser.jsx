// @flow

import * as React from 'react';
import Tree from 'react-tree-graph';
import type { Node } from '../node/Node';
import type { Folder } from '../node/Folder';
import { toggleNode } from '../node/Node';
import './FileTreeVisualiser.css';

/**
 * This type defines the structure of a
 * D3 hierarchy, which is required by the
 * {@link Tree} component.
 *
 * @see https://github.com/d3/d3-hierarchy/blob/master/README.md
 * @see https://github.com/jpb12/react-tree-graph
 */
type D3Hierarchy = {
  /**
   * The name of the hierarchy.
   */
  name: string,

  /**
   * Optional children definition.
   */
  children?: Array<D3Hierarchy>,

  /**
   * The unique identifier of the hierarchy.
   */
  identifier: string,
}

/**
 * This type represents the onClick callback function
 * that may be used with the {@link Tree} component.
 *
 * @param {Object} event - The object containing the event that triggered the function.
 * @param {string} nodeKey - The unique identifier of the hierarchy node that was clicked.
 */
type D3OnClick = (event: Object, nodeKey: string) => void;

/**
 * This type represents the props definition
 * of the {@link FileTreeVisualiser} component.
 */
type Props = {
  /**
   * The folder to visualise.
   */
  folder: ?Folder,

  /**
   * The width of the component.
   */
  width: number,

  /**
   * The height of the component.
   */
  height: number,
};

/**
 * This type represents the state definition
 * of the {@link FileTreeVisualiser} component.
 */
type State = {
  /**
   * The currently active folder structure (i.e. the folder to render).
   */
  activeFolder: ?Folder,
};

/**
 * Converts a {@link Node} to a {@link D3Hierarchy}.
 *
 * @param {Node} node - The {@link Node} to convert.
 * @returns {D3Hierarchy} - The resulting {@link D3Hierarchy}.
 */
function nodeToD3(node: ?Node): D3Hierarchy {
  // A define the name of the node and set a fallback value.
  let hierarchyName: string = '';
  // Define the D3Hierarchy object.
  const hierarchy: D3Hierarchy = { name: '', identifier: '' };

  // Make sure the node exists.
  if (!node) {
    return hierarchy;
  }

  // If the node is a folder.
  if (node.children && node.folderName) {
    // Store the name of the current node.
    hierarchyName = node.folderName;
    // Only map the children if the current folder is open.
    if (node.open) {
      // Map the children of the node
      // to a recursive call.
      // This will result in a traverse of the entire
      // tree of the initially passed in node.
      hierarchy.children = node.children.map((child: Node): D3Hierarchy => nodeToD3(child));
    }
  // If the node is a file.
  } else if (node.fileName) {
    // Store the name of the current node.
    hierarchyName = node.fileName;
  }

  // Set the name and identifier of the current hierarchy and return it.
  hierarchy.name = hierarchyName;
  hierarchy.identifier = node.identifier;
  return hierarchy;
}

/**
 * This component takes in a {@link Folder} and visualises it
 * graphically as a tree.
 */
export default class FileTreeVisualiser extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      activeFolder: props.folder,
    };
  }

  /**
   * Handles the event of a hierarchy node being clicked.
   * @see D3OnClick
   */
  handleNodeCLick: D3OnClick = (event: any, nodeKey: string): void => {
    const { activeFolder } = this.state;

    // Try to toggle the selected node and
    // force this component to rerender.
    if (activeFolder) {
      toggleNode(activeFolder, nodeKey);
      this.forceUpdate();
    }
  }

  render(): React.Node {
    // Destruct the state.
    const { activeFolder } = this.state;
    // Destruct the props;
    const { width, height } = this.props;

    // If no folder was passed in render a message.
    if (!activeFolder) {
      return <div>Nothing to visualise.</div>;
    }

    // Render the tree visualising the folder.
    // Note the use if the nodeToD3 method.
    return (
      <div className="file-tree-visualiser">
        <Tree
          data={nodeToD3(activeFolder)}
          width={width}
          height={height}
          animated
          keyProp="identifier"
          gProps={{ onClick: this.handleNodeCLick }}
        />
      </div>
    );
  }
}
