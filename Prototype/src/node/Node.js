// @flow

/**
 * This module describes and handles nodes.
 * A node is any point in a file tree.
 *
 * @module node/Node
 */

import type { File } from './File';
import type { Folder } from './Folder';
import { addPathToFolder, sortFolder, nodeToFolder } from './Folder';
import { nodeToFile } from './File';

/**
 * This type represents a node.
 * A node may be a {@link File} or a {@link Folder}.
 */
export type Node = File | Folder;

/**
 * This type represents a file path style.
 * A file path is a string that can be matched
 * using some defined regular expression.
 * A path style must contain a unified "separator" that
 * separates each node in the path.
 * A path style may optionally contain a (display) name.
 */
export type PathStyle = {
  /**
   * The name of the path style.
   */
  name?: string,

  /**
   * The regular expression used to determine
   * if a given file path string is "correct".
   */
  regex: RegExp,

  /**
   * The string pattern used to separate each node
   * in the file path string.
   */
  separator: string,
};

/**
 * This type represents a file path.
 * It is a convenient way of enforcing a
 * file path along with its respective path style
 */
export type Path = {
  /**
   * The string representing the file path.
   */
  pathString: string,

  /**
   * The respective path style.
   */
  pathStyle: PathStyle,
};

/**
 * This regex matches a path that separates the nodes by using slashes,
 * e.g. this/is/a/path.extension or /this/is/also/valid.extension.
 * The following rules should be kept in mind:
 *   * The nodes may only have alphanumeric letters, underscores and dashes in their names.
 *   * Each path must begin with a node name or a slash.
 *   * A slash in the beginning of a path marks the root of a file tree.
 *   * The last node in the path is considered a file and never a folder.
 */
const slashPathStyle: PathStyle = {
  name: 'slashPathStyle',
  regex: /^(?:[a-z]|\/)(?:[a-z_\-0-9.]+)(?:\/?[a-z_\-0-9.]+)+$/,
  separator: '/',
};

/**
 * An array caontining all supported path styles.
 */
const pathStyles: Array<PathStyle> = [
  slashPathStyle,
];

/**
 * Returns a possible {@link PathStyle} representing the passed in file path string.
 * If null is returned the passed in file path string is not supported or faulty.
 *
 * @param {string} path - The file path string to get a {@link PathStyle} for.
 * @returns {(PathStyle | null)} - The {@link PathStyle} if one is found, otherwise null.
 */
export function getPathStyle(path: string): PathStyle | null {
  // Stores the possibly matched path style.
  let matchedPathStyle: PathStyle | null = null;

  // Look through each supported path style.
  // If several path styles support the passed in path
  // the latest match is used.
  pathStyles.forEach((pathStyle: PathStyle) => {
    // Try to match the passed in path
    // using the regular expression of the path style.
    // If the regular expression matches exactly
    // with the path, the path style may be used.
    const match = path.match(pathStyle.regex);
    if (match && match[0] === path) {
      matchedPathStyle = pathStyle;
    }
  });

  // Return the potentially matched path style.
  return matchedPathStyle;
}

/**
 * Checks the validity of a given file path string.
 *
 * @param {string} path - The file path string to validate.
 * @returns {boolean} - True if the path is valid and false if not.
 */
export function isValidPath(path: string): boolean {
  // Try to get a path style for the path.
  const pathStyle: PathStyle | null = getPathStyle(path);

  // If a path style was found the path is valid,
  // otherwise it is invalid.
  return pathStyle !== null;
}

/**
 * Cleans a file path string in order to make easier to work with.
 *
 * @param {string} path - The file path string to clean.
 * @param {PathStyle} pathStyle - The {@link PathStyle} that the path follows.
 */
export function cleanPath(path: string, pathStyle: PathStyle): string {
  // If the passed in file path string starts with a separator remove it.
  if (path.charAt(0) === pathStyle.separator) {
    return path.substr(1);
  }

  // If nothing needs to be cleaned, simply return the original string.
  return path;
}

/**
 * Converts a flat array of string paths to a proper folder structure.
 *
 * @param {Array<string>} pathsArray - The array of paths to convert.
 * @param {string} rootName - The name of the root folder. The default is "/".
 * @returns {Folder} - An object following the Folder type.
 */
export function arrayToFolder(pathsArray: Array<string>, rootName: string = '/'): Folder {
  // Create the root folder.
  const folder: Folder = {
    folderName: rootName, children: [], identifier: rootName, open: true,
  };

  // Iterate over each passed in path.
  pathsArray.forEach((path: string) => {
    // Try to get the pathstyle of the passed in path.
    const pathStyle: PathStyle | null = getPathStyle(path);
    // If the path is valid (i.e. supported).
    if (pathStyle) {
      // Destruct it and add it to the root folder.
      addPathToFolder({ pathString: cleanPath(path, pathStyle), pathStyle }, folder);
    }
  });

  // Sort the resulting folder and return it.
  sortFolder(folder);
  return folder;
}

/**
 * Toggles the visibility (for files) / open state (for folders) of the matched node
 * within the passed in node based on the passed in string.
 *
 * Starting with the passed in node, this function will search for any node with a unique
 * identifier matching that of the passed in string (toggleIdentifier), and toggle the visibility
 * and open state, depending on if the matched node is a file or folder, respectively.
 *
 * @param {Node} node - The node to start the search with (haystack).
 * @param {string} toggleIdentifier - The unique identifier of the node to toggle (needle).
 */
export function toggleNode(node: Node, toggleIdentifier: string): void {
  const possibleFolder: Folder | null = nodeToFolder(node);

  // If the passed in node matches the search term:
  // open / close if the node is a folder and toggle the visibility
  // if the node is a file.
  if (node.identifier === toggleIdentifier) {
    // If the node is a file.
    const possibleFile: File | null = nodeToFile(node);
    if (possibleFile) {
      possibleFile.visible = !possibleFile.visible;
    }

    // If the node is a Folder.
    if (possibleFolder) {
      possibleFolder.open = !possibleFolder.open;
    }

    // Since a match was found, do not dig deeper.
    // Note that "neighbouring" calls will not stop.
    // This may have a slight performance implication (especially on large trees).
    // If performance becomes a concern consider adding
    // a "nodeFound" flag.
    return;
  }

  // If the node is a folder recursively look through the children.
  if (possibleFolder) {
    possibleFolder.children.forEach((child: Node) => {
      toggleNode(child, toggleIdentifier);
    });
  }
}
