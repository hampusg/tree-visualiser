import { arrayToFolder } from './Node';

const correctSlashStructure = [
  'one/two/three.png',
  'one/three/two-wait-what.png',
  '/start/with_slash.somethin',
  '/start/with/slash.again',
  'i-am-here-i-am-an-outer-file-get-used.toit',
];

const assureSlashStructure = {
  folderName: '/',
  identifier: '/',
  open: true,
  children: [
    {
      fileName: 'i-am-here-i-am-an-outer-file-get-used.toit',
      identifier: 'i-am-here-i-am-an-outer-file-get-used.toit',
      visible: true,
    },
    {
      folderName: 'one',
      identifier: 'one',
      open: true,
      children: [
        {
          folderName: 'three',
          identifier: 'one/three',
          open: true,
          children: [
            {
              fileName: 'two-wait-what.png',
              identifier: 'one/three/two-wait-what.png',
              visible: true,
            },
          ],
        },
        {
          folderName: 'two',
          identifier: 'one/two',
          open: true,
          children: [
            {
              fileName: 'three.png',
              identifier: 'one/two/three.png',
              visible: true,
            },
          ],
        },
      ],
    },
    {
      folderName: 'start',
      identifier: 'start',
      open: true,
      children: [
        {
          fileName: 'with_slash.somethin',
          identifier: 'start/with_slash.somethin',
          visible: true,
        },
        {
          folderName: 'with',
          identifier: 'start/with',
          open: true,
          children: [
            {
              fileName: 'slash.again',
              identifier: 'start/with/slash.again',
              visible: true,
            },
          ],
        },
      ],
    },
  ],
};

const incorrectStructure = [
  'partial/filepath/space and stuff',
  'incorrect\\filepath\\space and all',
];

const assureIncorrectStructure = {
  folderName: '/',
  identifier: '/',
  open: true,
  children: [],
};

test('correctly converts file structure', () => {
  const result = arrayToFolder(correctSlashStructure);
  expect(result).toEqual(assureSlashStructure);
});

test('ignores incorrect file structure', () => {
  const result = arrayToFolder(incorrectStructure);
  expect(result).toEqual(assureIncorrectStructure);
});
