// @flow

/**
 * This module describes and handles folders.
 * A folder is a type of node.
 *
 * @module node/Folder
 */

import type { File } from './File';
import type { Node, Path } from './Node';

import { nodeIsFile, nodeToFile } from './File';

/**
 * This type represents a folder.
 * A folder is a  {@link Node} that has a name and children.
 */
export type Folder = {|
  /**
   * The name of the folder.
   */
  folderName: string,

  /**
   * The children of the folder.
   */
  children: Array<Node>,

  /**
   * The unique identifier of the folder.
   */
  identifier: string,

  /**
   * The open state of the folder. i.e. open or closed.
   */
  open: boolean,
|};

/**
 * Given a {@link Node} this function returns a {@link Folder} if
 * applicable, and null otherwise.
 *
 * @param {Node} node - The {@link Node} to convert.
 * @returns {(Folder | null)} - The possible {@link Folder} or null.
 */
export function nodeToFolder(node: Node): Folder | null {
  // Only return the possible folder if all required properties exist.
  if (node && node.folderName && node.children && node.identifier && 'open' in node) {
    return node;
  }

  return null;
}

/**
 * Get the name of a given {@link Node}.
 *
 * @param {Node} node - The {@link Node} to get the name of.
 * @returns {string} - The name of the {@link Node}.
 */
export function getNodeName(node: Node): string {
  // If the node is a File.
  const possibleFile: File | null = nodeToFile(node);
  if (possibleFile) {
    return possibleFile.fileName;
  }

  // If the node is a Folder.
  const possibleFolder: Folder | null = nodeToFolder(node);
  if (possibleFolder) {
    return possibleFolder.folderName;
  }

  return '';
}

/**
 * Checks if the passed in {@link Node} is of type {@link Folder}.
 *
 * @param {Node} node - The {@link Node} to check.
 * @returns {boolean} - True if the node is of type {@link Folder}, false if not.
 */
function nodeIsFolder(node: Node): boolean {
  // Try to convert the node to a folder.
  const folder = nodeToFolder(node);
  // If the conversion was succeful, the node is a folder.
  if (folder) {
    return true;
  }

  return false;
}

/**
 * Sorts a {@link Folder} and all its children.
 * {@link File}s have precedence over {@link Folder}s,
 * meaning that they will be placed first.
 * Note that this method modifies the passed in {@link Folder}.
 *
 * @param {(Folder | null)} folder - The {@link Folder} to sort (null will be ignored).
 */
export function sortFolder(folder: Folder | null): void {
  // If null is passed down. This mainly exists
  // in order to avoid having to "null check"
  // the nodeToFolder call further down.
  if (folder === null) {
    return;
  }

  // Sort the children of the passed in folder.
  folder.children.sort((a: Node, b: Node) => {
    const aName: string = getNodeName(a);
    const bName: string = getNodeName(b);
    const aIsFile: boolean = nodeIsFile(a);
    const bIsFile: boolean = nodeIsFile(b);

    // If "a" is a file and "b" is not
    // "a" should be placed first.
    if (aIsFile && !bIsFile) {
      return -1;
    }

    // If "b" is a file and "a" is not
    // "b" should be placed first.
    if (!aIsFile && bIsFile) {
      return 1;
    }

    // Place "a" before "b".
    if (aName < bName) {
      return -1;
    }

    // Place "b" before "a".
    if (aName > bName) {
      return 1;
    }

    // Leave the choice up to the interpreter.
    return 0;
  });

  // Check each child of the folder.
  folder.children.forEach((child: Node) => {
    // Only folder may be sorted.
    if (nodeIsFolder(child)) {
      // Recursively call this method with
      // the child folders.
      sortFolder(nodeToFolder(child));
    }
  });
}

/**
 * Add a string representing a path to a {@link file} to a given {@link Folder}.
 * The passed in path must be a valid {@link PathStyle}, otherwise it will be ignored.
 * Note that this method modifies the passed in {@link Folder}.
 *
 * See the {@link './Node.js'}
 *
 * @param {string} path - The path to add. This must be a valid {@link PathStyle}.
 * @param {Folder} folder - The folder to add the path to.
 */
export function addPathToFolder(path: Path, folder: Folder): void {
  // Split the path using the separator of the path style,
  // in order to get an array containing the nodes of the path.
  const splitPath: Array<string> = path.pathString.split(path.pathStyle.separator);
  // This holds the current active folder (i.e. the folder to place new nodes in).
  // It changes with the passed in path. E.g. given the path /some/random/path.txt
  // the current folder will follow this path as follows: some -> random.
  let currentFolder: Folder = folder;

  // Traverse the nodes of the path.
  splitPath.forEach((nodeName: string, pathIndex: number) => {
    // Set to true if the current node is the last node (i.e. the file).
    const isLastNode = pathIndex === splitPath.length - 1;
    // Try to find the current node name in the current folder.
    // i.e. the node already exists!
    // This variable will be set to the index of a potential match,
    // and -1 if no match is found.
    const nodeMatchIndex = currentFolder.children.findIndex(
      (child: File | Folder) => {
        // If the current child is a File.
        const possibleFile: File | null = nodeToFile(child);
        if (possibleFile) {
          return possibleFile.fileName === nodeName;
        }

        // If the current child is a Folder.
        const possibleFolder: Folder | null = nodeToFolder(child);
        if (possibleFolder) {
          return possibleFolder.folderName === nodeName;
        }

        return false;
      },
    );

    // If the current node (node name) already exists,
    // special handling is required.
    if (nodeMatchIndex > -1) {
      // Find out if the matched node is a folder.
      const possibleFolder = nodeToFolder(currentFolder.children[nodeMatchIndex]);
      // If it is a folder, set it to the current working folder.
      if (possibleFolder) {
        currentFolder = possibleFolder;
      }

      // Skip to the next node in the path.
      return;
    }

    // Create a unique identifier for the node.
    const identifier = splitPath.slice(0, pathIndex + 1).join(path.pathStyle.separator);

    // If this is the last node (i.e. a file).
    if (isLastNode) {
      // Create a new file and add it to the current folder.
      const newFile: File = { fileName: nodeName, identifier, visible: true };
      currentFolder.children.push(newFile);
    // If this is not the last node (i.e. a folder).
    } else {
      // Create a new folder and add it to the current folder.
      const newFolder: Folder = {
        folderName: nodeName, children: [], identifier, open: true,
      };
      currentFolder.children.push(newFolder);
      currentFolder = newFolder;
    }
  });
}
