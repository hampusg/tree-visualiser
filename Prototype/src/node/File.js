// @flow

/**
 * This module describes and handles files.
 * A file is a type of node.
 *
 * @module node/File
 */

import type { Node } from './Node';

/**
 * This type represents a file.
 * A file is a  {@link Node} that has a name.
 */
export type File = {|
  /**
   * The name of the file.
   */
  fileName: string,

  /**
   * The unique identifier of the file.
   */
  identifier: string,

  /**
   * The visibility state of the file. i.e. visible or hidden.
   */
  visible: boolean,
|};

/**
 * Given a {@link Node} this function returns a {@link File} if
 * applicable, and null otherwise.
 *
 * @param {Node} node - The {@link Node} to convert.
 * @returns {(File | null)} - The possible {@link File} or null.
 */
export function nodeToFile(node: Node): File | null {
  // Only return the possible file if all required properties exist.
  if (node && node.fileName && node.identifier && 'visible' in node) {
    return node;
  }

  return null;
}

/**
 * Checks if the passed in {@link Node} is of type {@link File}.
 *
 * @param {Node} node - The {@link Node} to check.
 * @returns {boolean} - True if the node is of type {@link File}, false if not.
 */
export function nodeIsFile(node: Node): boolean {
  // Only return true if all required properties exist.
  if (node && node.fileName) {
    return true;
  }

  return false;
}
