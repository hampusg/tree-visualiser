// @flow

import * as React from 'react';
import './App.css';
import Body from './component/Body';
import FileTreeVisualiser from './component/FileTreeVisualiser';
import { arrayToFolder } from './node/Node';

const exampleArray = [
  'minafiler/foton/selfie.png',
  'minafiler/foton/selfie-tva.png',
  'marvel/black_widow/bw.png',
  'marvel/drdoom/the-doctor.png',
  'fact_marvel_beats_dc.txt',
  'dc/aquaman/mmmmmomoa.png',
  'marvel/black_widow/why-the-widow-is-awesome.txt',
  'dc/aquaman/movie-review-collection.txt',
  'marvel/marvel_logo.png',
  'dc/character_list.txt',
  '/start/with_slash.somethin',
  'partial/filepath/space and stuff',
  'incorrect\\filepath\\space and all',
];

function App(): React.Node {
  // Setup the state.
  const [size, setSize] = React.useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  // Setup an event listener that changes the
  // size state of this component whenever
  // the "window" object is resized.
  React.useEffect(() => {
    window.addEventListener('resize', () => {
      setSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    });
  });

  const fileTreeWidth = size.width;
  const fileTreeHeight = size.height;

  return (
    <Body>
      <FileTreeVisualiser
        width={fileTreeWidth}
        height={fileTreeHeight}
        folder={arrayToFolder(exampleArray)}
      />
    </Body>
  );
}

export default App;
