const os = require('os');

module.exports = {
  "env": {
    "browser": true,
    "jest/globals": true
  },
  "extends": [
    "airbnb",
    "plugin:react/recommended",
    "plugin:jsx-a11y/recommended",
    "plugin:flowtype/recommended"
  ],
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    },
    "sourceType": "module"
  },
  "plugins": [
    "react",
    "jsx-a11y",
    "jest",
    "flowtype"
  ],
  "ignorePatterns": [ "build/" ],
  "rules": {
    "react/jsx-filename-extension": [
      "error",
      {
        "extensions": [ ".test.js", ".jsx" ]
      }
    ],
    "linebreak-style": [ "error", os.EOL === "\r\n" ? "windows" : "unix" ]
  }
};
