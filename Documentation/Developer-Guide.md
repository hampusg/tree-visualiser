# Developer Guide for Tree Visualiser

Author: Hampus Gunnrup
<br />
Created: 2020-10-14
<br />
Updated: 2020-10-14

## Table of Contents

- [Developer Guide for Tree Visualiser](#developer-guide-for-tree-visualiser)
  - [Table of Contents](#table-of-contents)
  - [About This Document](#about-this-document)
  - [Workflow](#workflow)
    - [1. User Story and Requirements](#1-user-story-and-requirements)
    - [2. Branch Creation](#2-branch-creation)
    - [3. User Case and Mock-up](#3-user-case-and-mock-up)
    - [4. Design](#4-design)
    - [5. Test Driven Development](#5-test-driven-development)
    - [6. Revision](#6-revision)
    - [7. Integration](#7-integration)

## About This Document

This document proposes a unified and simple workflow to follow when contributing to this project.

## Workflow

The defined workflow is designed for one person or a small team, and is inspired by known software engineering practices, especially agile ones.

The defined steps in this section are based on the assumption that a feature is to be implemented. However, not all work is based on features. In these cases, as many of the steps as possible should be used. Further, if needed, the steps may be modified. Examples of other types of work are fixing bugs, integrating new tools, modifying the documentation and updating the pipeline.

Due note that it is desirable that the workflow is followed as closely as possible, even in odd scenarios. Each anomaly should be justified.

### 1. User Story and Requirements

The first thing that should be done in a workflow, is to create one or more user stories. This should be done in a manner according to Scrum. Each user story should represent an entire feature, meaning that it should not focus on specific parts like user interface design, back-end, architecture, application logic etc. A user story may already exist for the feature, in that case this step can be skipped.

Notice how we go directly into user story creation and skip the epics that are typically part of Scrum. This is because we want to be effective, and defining epics is not necessarily helpful for single developers or small teams. In fact, there are a lot of aspects of Scrum that are not being used in this workflow. For instance, burn downs, estimations, two week sprints and scrum meetings are all things that are not required (although certainly allowed if needed) in this workflow.

### 2. Branch Creation

When you start working on a user story, always create a new branch (from master) for it. All work that is made towards completing the user story should be committed to this branch and this branch only.

### 3. User Case and Mock-up

Before doing any work, a simple use case should be created. Further, for any important requirement alternate paths should be defined, in order to document complex interactions.

For some user stories, requirements (quality attribute requirements) may be defined along with the use cases. These should regard quality attributes that are hard to define through use cases. Examples of quality attributes are security, performance and maintainability. Furthermore, other types of requirements documentation may have been created. For instance, a set of function requirements (MoSCoW). In these cases, those documents should be updated as needed in this step.

Along with the use cases a UI mock ups should be created. It does not have to be very detailed, but should serve as a good documentation of the feature. See the recommended toolchain for the appropriate tool for this task.

The use cases along with the UI mock ups serve as the functional requirements. They communicate what the software should do.

### 4. Design

After creating a use case description, the user story/feature should be designed and added to the architecture project. This should be done in two stages:

1. Create the module structure. This might mean adding the design to an existing or new class diagram(s) and/or component diagram(s).
2. Create an appropriate runtime structure. This might mean adding a sequence diagram for each use case scenario that is defined for the user story/feature, this may include alternate paths.

### 5. Test Driven Development

This step is the actual implementation step. Simply start by creating a test, preferably a integration test, that covers the main part of the requirements for the user story/feature. After creating the test (and stubs to prevent a crash) run it and make sure it fails. Now, implement the code necessary in order for the test to pass. Repeat this cycle with more detailed unit tests until all the requirements (from the use cases and quality attribute requirements) of the user story are fulfilled. If test driven development does not fit the specific user story, the test cases may be implemented afterwards as you see fit.

### 6. Revision

Finally, you should revise everything that has been done. This may include going through the user story, use cases and possible quality attribute requirements to make sure that every requirement is covered; rethinking the software design; and finally extensively testing the implementation, through manual (or automatic) GUI tests.

### 7. Integration

The final step is to simply integrate the changes into the working software. This means that you have to merge your changes into the master branch. As a precaution it is better to instead merge the master branch into your working branch, test the integration and fix the possible issues. After successfully merging the master branch into your branch, you can merge your branch into master and trust that everything is fine. Lastly, remove the branch that you were working on, as it is no longer needed.
