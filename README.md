# Tree Visualiser

Author: Hampus Gunnrup
<br />
Created: 2020-10-02
<br />
Updated: 2024-09-05

## Table of Contents

- [Tree Visualiser](#tree-visualiser)
  - [Table of Contents](#table-of-contents)
  - [About This Project](#about-this-project)
  - [Technology Stack](#technology-stack)
    - [Reasoning](#reasoning)
    - [Tools](#tools)
      - [Required](#required)
      - [Recommended](#recommended)
  - [Roadmap](#roadmap)
  - [Design Decision](#design-decision)
  - [How Do I Contribute?](#how-do-i-contribute)
  - [The Structure of This Project](#the-structure-of-this-project)
  - [Useful Links](#useful-links)
    - [React](#react)
    - [Deployment](#deployment)
    - [Etc](#etc)

## About This Project

Tree Visualiser aims to solve the problem of visualising a file tree structure.

## Technology Stack

Tree Visualiser uses ReactJS to create an application entirely in Javascript.

The goal is to create a completely abstracted application that can be bootstrapped into any platform using tools such as Electron, React Native, Gatsby and Ionic. This creates the possiblity of having true cross-platform deployment using the same codebase for all platforms.

### Reasoning

The reasoning behind the stack is that Javascript has a fast growing ecosystem and an already well-established platform for Frontend development. With the rise of NodeJS and Javascript bindings for various development platforms, Javascript becomes the language that has the most power. There is great power and convenience in using the same programming language in all components of the stack.

ReactJS was chosen since it is one of the most popular Javascript frameworks. Which means that it has a large ecosystem around it. Applications created with ReactJS are easily ported to multiple platforms using tools such as React Native, Ionic and Electron.

### Tools

#### Required

* [Node.js](https://nodejs.org).
* [Npm](https://www.npmjs.com/get-npm).

#### Recommended

* VSCode with the following plugins:
  * flowtype/Flow Language Support.
  * dbaeumer/ESLint.
  * jebbs/PlantUML.
  * yzhang/Markdown All in One.
* Gimp.

## Roadmap

Taking the goals of this project into account, the following high-level roadmap has been established:

1. Prototype. Built with the technology stack in mind.
2. Base application, functional in all major webbrowsers (including mobile platforms).
3. Native apps deployed on iOS and Android.
4. Deployment to other platforms such as AndroidTV and Wearble devices.

The idea is to first work on the main goals of the project by creating a prototype that can later be used as the base of the application. Secondly the problem of deploying to different platforms should be solved.

## Design Decision

* The source of the file tree is irrelevant and should be completely abstracted.
* The rendering of the UI should be completely abstracted (in other words: do not use tags such as `<div>` and `<ul>`).
* Use design principles such as Proxies, Wrappers and Adapters in order to make the project more "bootstrappable".

## How Do I Contribute?

Before contributing, it is advicable to take a look at the developer guide located at [`<project-root>/Documentation/Developer-Guide.md`](https://gitlab.com/hampusg/tree-visualiser/-/blob/master/Documentation/Developer-Guide.md). This document presents a simple software process that should be followed.

A backlog is available in the form of boards at https://gitlab.com/hampusg/tree-visualiser/-/boards. Further, a full set of issues to solve is available at: https://gitlab.com/hampusg/tree-visualiser/-/issues

On a final note, you may always contact gunnrup@gmail.com for more information.

## The Structure of This Project

The main structure of this project is as following:

* Documentation: stores all documentation of the project.
  * Architecture: stores all software architecture and design related documents.
  * UX: stores all user interface related assets and documents.
* Prototype: houses the prototype of the project.

The project is hosted on Gitlab at https://gitlab.com/eloquent-projects/jumper-game and can be accessed live via http://tree-visualiser.eloquent.se/ and https://tree-visualiser-prototype.herokuapp.com/.

A CI / CD chain can be found at [`<project-root>/.gitlab-ci.yml`](https://gitlab.com/hampusg/tree-visualiser/-/blob/master/.gitlab-ci.yml).

## Useful Links

### React

* https://create-react-app.dev
* https://blog.usejournal.com/creating-a-react-app-from-scratch-f3c693b84658?gi=ffd356793384
* https://medium.com/@pppped/extend-create-react-app-with-airbnbs-eslint-config-prettier-flow-and-react-testing-library-96627e9a9672
* https://hackernoon.com/structuring-projects-and-naming-components-in-react-1261b6e18d76

### Deployment

* https://create-react-app.dev/docs/deployment
* https://github.com/mars/create-react-app-buildpack
* https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4
* https://github.com/travis-ci/dpl
* https://docs.gitlab.com/ee/ci/examples/deployment/
* https://docs.gitlab.com/ee/ci/pipelines/
* https://devcenter.heroku.com/articles/custom-domains

### Etc

* https://ionicframework.com/docs/core-concepts/cross-platform
* https://flow.org/en/docs/react
* https://eslint.org/docs/user-guide/configuring
